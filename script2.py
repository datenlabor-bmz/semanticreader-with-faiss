import pandas as pd
import numpy as np
import pickle 
from transformers import AutoTokenizer, AutoModel
import torch
import torch.nn.functional as F
import dash_bootstrap_components as dbc
from dash import Dash, html, dcc, Input, Output, dash_table, State

df = pd.read_csv("2021.csv")
df = df[df["Long Description"].notna()]
df = df[df["Long Description"].str.len() > 80]

from faiss import read_index
index = read_index("index.bin")

with open('doc_map.pkl', 'rb') as f:
    doc_map = pickle.load(f)

class SemanticEmbedding:

    def __init__(self, model_name='sentence-transformers/all-mpnet-base-v2'):
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.model = AutoModel.from_pretrained(model_name)
    
    #Mean Pooling - Take attention mask into account for correct averaging
    def mean_pooling(self, model_output, attention_mask):
        token_embeddings = model_output[0] #First element of model_output contains all token embeddings
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(input_mask_expanded.sum(1), min=1e-9)

    def get_embedding(self, sentences):
        # Tokenize sentences
        encoded_input = self.tokenizer(sentences, padding=True, truncation=True, return_tensors='pt')
        with torch.no_grad():
            model_output = self.model(**encoded_input)
        # Perform pooling
        sentence_embeddings = self.mean_pooling(model_output, encoded_input['attention_mask'])

        # Normalize embeddings
        sentence_embeddings = F.normalize(sentence_embeddings, p=2, dim=1)
        return sentence_embeddings.detach().numpy()

model = SemanticEmbedding()

def search_doc( query, k=50):
  D, I = index.search(model.get_embedding(query), k)
  table = pd.DataFrame(columns = ["Long Description", "Score"])
  for i in range(len(I[0])):
    table.loc[i, "Long Description"] = doc_map[I[0][i]]
    table.loc[i, "Score"] = D[0][i]
  return table

app = Dash(external_stylesheets = [dbc.themes.SUPERHERO])

dat = pd.DataFrame()

app.layout = dbc.Container([
     dbc.Row([
         html.H1(children='Dashboard Semantic Search', style={'textAlign': 'center'}),
         html.P("Enter a search term and select the number of closest matches that should be displayed via the slider.\
                 It suffices to enter short keywords, i.e. taxation reform or feminist foreign policy. The program\
                 will scan the project descriptions of the ODA data for 2021 and give the most relevant matches based on semantic similarity.\
                 The score indicates the relevance of the project descriptions.", 
                style={'textAlign': 'center'})
      ]),

    html.Div([
            dcc.Input(id="input1", type="text", value='', placeholder="Enter a search term",
                           size="50", className="mb-3"),
    ], style = dict(display='flex', justifyContent='center')),
    
    html.Div([
       dcc.Slider(10, 2000, step = None, value = 10,
               marks = {10: "10", 100: "100", 200: "200", 
                        500: "500", 1000: "1000", 2000: "2000"},
               id='my-slider')
               ], style = {"width": "100%", "align-items": "center", "justify-content": "center"}),
        
    html.Div([
       html.Button('Search', id='button', className="btn btn-info"),
    ],  style = dict(display='flex', justifyContent='center')),

    dbc.Row([
        my_table := dash_table.DataTable(
        dat.to_dict('records'), [{"name": i, "id": i} for i in dat.columns],
        filter_action="native", sort_action="native", page_size= 200,
        style_data={'color': 'black', 'backgroundColor': 'white'},
        style_cell={'textAlign': 'left', "whiteSpace": "normal", "height": "auto"},
         style_header={'backgroundColor': 'rgb(11, 148, 153)', 'color': 'black', 'fontWeight': 'bold'},
             style_data_conditional=[{
            'if': {'row_index': 'odd'},
            'backgroundColor': 'rgb(235, 240, 240)',
        }], export_format= "xlsx"),
         ]),
])


@app.callback(
    [Output(my_table, 'data'), Output(my_table, "columns")],
    [Input('button', 'n_clicks')],
    [State('input1', 'value'), State("my-slider", "value")]
)

def update_output(n_clicks, txt, slider):

  if (txt != None) & (txt != ""): 
     table = search_doc(txt, slider)
  else:
     table = pd.DataFrame()

  row = table.to_dict("records")
  col = [{"name": i, "id": i} for i in table.columns]

  return row, col

if __name__ == '__main__':
    app.run_server(debug=True)